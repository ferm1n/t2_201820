package api;

import model.data_structures.IDoublyLinkedList;
import model.vo.VOStation;
import model.vo.VOTrip;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager {

	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 */
	void loadTrips(String tripsFile);
	
	void loadStations(String stationsFile);
	/**
	 * Method to load the Divvy Stations of the STS
	 * @param stationsFile - path to the file 
	 */

	

	public IDoublyLinkedList <VOTrip> getTripsOfGender (String gender);
	
	
	public IDoublyLinkedList <VOTrip> getTripsToStation (int stationID);


	
}
