
package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.IDoublyLinkedList;
import model.vo.VOStation;
import model.vo.VOTrip;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			IDoublyLinkedList<VOTrip> bykeTripsList;
			
			switch(option)
			{
				case 1:
					Controller.loadStations();
					break;
					
				case 2:
					Controller.loadTrips();
					break;
					
				case 3:
					System.out.println("Ingrese el genero:");
					String gender = sc.next();
					bykeTripsList = Controller.getTripsOfGender (gender);
					System.out.println("Se encontraron "+ bykeTripsList.getSize() + " elementos");
					for (VOTrip trips : bykeTripsList) 
					{
						System.out.println(trips.id() + " " + trips.getTripSeconds() + " " + trips.getFromStation() + " " + trips.getToStation());;
					}
					break;
					
				case 4:
					System.out.println("Ingrese el identificador de la estaci�n:");
					int stationId = Integer.parseInt(sc.next());
					bykeTripsList = Controller.getTripsToStation(stationId);
					System.out.println("Se encontraron " + bykeTripsList.getSize() + " elementos");
					for (VOTrip trips : bykeTripsList) 
					{
						System.out.println(trips.id() + " " + trips.getTripSeconds() + " " + trips.getFromStation() + " " + trips.getToStation());
					}
					break;
					
				case 5:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 2----------------------");
		System.out.println("1. Cree una nueva coleccion de estaciones");
		System.out.println("2. Cree una nueva coleccion de viajes");
		System.out.println("3. Dar listado de viajes realizados dado un genero");
		System.out.println("4. Dar listado de viajes que finalizan en una estaci�n de bicicletas espec�fica");
		System.out.println("5. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
