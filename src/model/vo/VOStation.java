package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representation of a station object
 */
public class VOStation {

	/**
	 * @return id_station - Station_id
	 */
	private int id;
	private String nombre;
	private String ciudad;
	private double latitud;
	private double longitud;
	private int capacidad;
	private Date fecha;
	
	public VOStation(String pId, String pNombre, String pCiudad, String pLatitud, String pLongitud, String pCapacidad, String pFecha){
		id=Integer.parseInt(pId);
		nombre=pNombre;
		ciudad=pCiudad;
		latitud=Double.parseDouble(pLatitud);
		longitud=Double.parseDouble(pLongitud);
		capacidad=Integer.parseInt(pCapacidad);
		SimpleDateFormat fecha1=new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {
			fecha= fecha1.parse(pFecha);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}
	public int getId() {
		return id;
	}
	public String getNombre() {
		return nombre;
	}
	public String getCiudad() {
		return ciudad;
	}
	public double getLatitud() {
		return latitud;
	}
	public double getLongitud() {
		return longitud;
	}
	public int getCapacidad() {
		return capacidad;
	}
	public Date getFecha() {
		return fecha;
	}
	public int id() {
		// TODO Auto-generated method stub
		return 0;
	}	
}
