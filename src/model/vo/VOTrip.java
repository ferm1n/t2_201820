package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representation of a Trip object
 */
public class VOTrip {
	
	private int id;
	private Date tiempoInicio;
	private Date tiempoFinal;
	private int bikeId;
	private int duracionViaje;
	private int idEstacionInicio;
	private String nombreEstacionInicio;
	private int idEstacionFin;
	private String nombreEstacionFin;
	private String tipoUsuario;
	private String genero;
	private int anhoNacimiento;
	
	public VOTrip(String pId, String pInicio, String pFinal, String pBikeId, String pDuracionViaje, String pIdEstacionInicio, String pNombreEstacionInicio, String pIdEstacionFin,
			String pNombreEstacionFin, String pTipoUsuario, String pGenero, String pAnhoNacimiento){
		id=Integer.parseInt(pId);
		bikeId=Integer.parseInt(pBikeId);
		duracionViaje=Integer.parseInt(pDuracionViaje);
		idEstacionInicio=Integer.parseInt(pIdEstacionInicio);
		nombreEstacionInicio=pNombreEstacionInicio;
		idEstacionFin=Integer.parseInt(pIdEstacionFin);
		nombreEstacionFin=pNombreEstacionFin;
		tipoUsuario=pTipoUsuario;
		genero=pGenero;
		anhoNacimiento=Integer.parseInt(pAnhoNacimiento);
		SimpleDateFormat fecha1=new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {
			tiempoInicio= fecha1.parse(pInicio);
			tiempoFinal= fecha1.parse(pFinal);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}
	
	@Override
	public String toString() {
		return   id + ", tiempoInicio=" + tiempoInicio + ", tiempoFinal=" + tiempoFinal + ", bikeId="
				+ bikeId + ", duracionViaje=" + duracionViaje + ", idEstacionInicio=" + idEstacionInicio
				+ ", nombreEstacionInicio=" + nombreEstacionInicio + ", idEstacionFin=" + idEstacionFin
				+ ", nombreEstacionFin=" + nombreEstacionFin + ", tipoUsuario=" + tipoUsuario + ", genero=" + genero
				+ ", anhoNacimiento=" + anhoNacimiento + "]";
	}

	public int getId() {
		return id;
	}

	public Date getTiempoInicio() {
		return tiempoInicio;
	}

	public Date getTiempoFinal() {
		return tiempoFinal;
	}

	public int getBikeId() {
		return bikeId;
	}

	public int getDuracionViaje() {
		return duracionViaje;
	}

	public int getIdEstacionInicio() {
		return idEstacionInicio;
	}

	public String getNombreEstacionInicio() {
		return nombreEstacionInicio;
	}

	public int getIdEstacionFin() {
		return idEstacionFin;
	}

	public String getNombreEstacionFin() {
		return nombreEstacionFin;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public String getGenero() {
		return genero;
	}

	public int getAnhoNacimiento() {
		return anhoNacimiento;
	}

	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return 0;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return "";
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return "";
	}
}
