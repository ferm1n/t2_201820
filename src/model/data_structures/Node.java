package model.data_structures;

public class Node<F>{
	private F dato;
	private Node<F> siguiente;
	private Node<F> anterior;
	
	public Node(F pDato){
		dato=pDato;
		siguiente=null;
		anterior=null;
	}
	
	public Node(F pDato, Node<F> pSig, Node<F> pAnt)
	{
		dato=pDato;
		siguiente=pSig;
		anterior=pAnt;
	}
	
	public void cambiarSiguiente(Node<F> pSig){
		siguiente=pSig;
	}
	
	public void cambiarAnterior(Node<F> pAnt){
		anterior=pAnt;
	}
	
	public Node<F> darSiguiente(){
		return siguiente;
	}
	
	public Node<F> darAnterior(){
		return anterior;
	}
	
	public void cambiarDato(F pDato){
		dato=pDato;
	}
	
	public F darDato(){
		return dato;
	}
}
