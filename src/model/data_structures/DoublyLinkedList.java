package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<F> implements IDoublyLinkedList<F> {
	
	private Node<F> primero;

	
	public DoublyLinkedList(F pRaiz)
	{
		primero=new Node<F>(pRaiz);
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		Node<F> actual=null;
		int contador=1;
		if(primero==null){
			return 0;
		}else{
			actual=primero;
			while(actual.darSiguiente()!=null){
				contador++;
				actual=actual.darSiguiente();
			}
		}
		return contador;
	}

	@Override
	public Iterator<F> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * Agrega el dato ingresado por parametro en el primer nodo.
	 * 
	 */
	@Override
	public void agregar(F pDato) {
		// TODO Auto-generated method stub
		Node<F> nuevo= new Node(pDato);
		if(primero==null)
		{
			primero=nuevo;
		
		}else{
			Node<F> temp=primero;
			primero=nuevo;
			nuevo.cambiarSiguiente(temp);
		
		}
	}

	@Override
	public void agregarAlFinal(F pDato) {
		// TODO Auto-generated method stub
		Node<F> actual=null;
		Node<F> nuevo=new Node(pDato);
		if(primero==null)
		{
			primero=nuevo;
		}else{
			actual=primero;
			while(actual.darSiguiente()!=null)
			{
				actual=actual.darSiguiente();
			}
			actual.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(actual);
		}
	}



	@Override
	public F darElemento(int pPos) {
		// TODO Auto-generated method stub
		if(primero!=null && pPos<=getSize())
		{
			Node<F> actual=primero;
			for(int i=1; i<pPos-1; i++){
				actual=actual.darSiguiente();					
			}
			return actual.darDato();
		}else{
			return null;
		}
	}


	@Override
	public void borrar(F pDato) {
		// TODO Auto-generated method stub
		if(primero!=null)
		{
			if(primero.darDato().equals(pDato))
			{
				Node<F>  ant=null, sig=null;
				primero.darSiguiente().cambiarAnterior(null);;
				primero=sig;
		
			}else{
				Node<F> actual=primero, ant=null, sig=null;
				boolean termino=false;
				while(actual.darSiguiente()!=null && !termino)
				{
					if(actual.darDato().equals(pDato))
					{
						ant=actual.darAnterior();
						sig=actual.darSiguiente();
						ant.cambiarSiguiente(sig);
						sig.cambiarAnterior(ant);
						termino=true;
					}else{
						actual=actual.darSiguiente();
					}
				}
				if(actual.darSiguiente()==null && actual.darDato().equals(pDato)){
					actual.darAnterior().cambiarSiguiente(null);
					
				}
			}
		}
	}

@Override
public F buscarElemento(F pElemento) {
	// TODO Auto-generated method stub
	F buscado=null;
	if(primero!=null)
	{
		if(primero.darDato().equals(pElemento))
		{
			buscado=primero.darDato();
		}else{
			Node<F> actual=primero;
			while(actual.darSiguiente()!=null)
			{
				if(actual.darDato().equals(pElemento))
				{
					buscado=actual.darDato();
				}else{
					actual=actual.darSiguiente();
				}
			}
			if(actual.darSiguiente()==null && actual.darDato().equals(pElemento)){
				buscado=actual.darDato();
			}
		}
	}
	return buscado;
}



}
