package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<F> extends Iterable<F> {
	
	
	int getSize();
	
	void agregar(F dato);
	
	void agregarAlFinal(F dato);
	
	//void agregarEnK(F dato, int ubicacion);
	
	F darElemento(int pos);
	
	F buscarElemento(F elemento);
	
	//F darElementoActual();
	
	void borrar(F dato);
	
	//void borrarEnK();

}
