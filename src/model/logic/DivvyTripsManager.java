package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;

public class DivvyTripsManager<F> implements IDivvyTripsManager {
	private DoublyLinkedList<F> viajes;
	private DoublyLinkedList<F> estaciones;

	
	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub
		try {
			BufferedReader br = new BufferedReader(new FileReader(stationsFile));
			String linea = null;
			linea=br.readLine();
			linea=br.readLine();
			String[] datos=linea.split(",");
			VOStation estacion=new VOStation(datos[0], datos[1], datos[2], datos[3], datos[4], datos[5], datos[6]);
			estaciones = new DoublyLinkedList(estacion);
			while ((linea = br.readLine()) != null) {
				datos=linea.split(",");
			    estaciones.agregar((F) new VOStation(datos[0], datos[1], datos[2], datos[3], datos[4], datos[5], datos[6]));
			    System.out.println(linea);
			}
	        br.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Se produjo un errosr leyendo los datos");
        }
	}

	
	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		try {
			BufferedReader br = new BufferedReader(new FileReader(tripsFile));
			String linea = null;
			linea=br.readLine();
			linea=br.readLine();
			String[] datos=linea.split(",");
			VOTrip viaje=new VOTrip(datos[0], datos[1], datos[2], datos[3], datos[4], datos[5], datos[6], datos[7], datos[8], datos[9], datos[10], datos[11]);
			viajes = new DoublyLinkedList(viaje);
			while ((linea = br.readLine()) != null) {
				datos=linea.split(",");
				if(datos.length==10)
				{
					System.out.println(linea);
					viajes.agregar((F) new VOTrip(datos[0], datos[1], datos[2], datos[3], datos[4], datos[5], datos[6], datos[7], datos[8], datos[9], "No dado", "0"));
				}else if(datos.length==11)
				{
					System.out.println(linea);
					viajes.agregar((F) new VOTrip(datos[0], datos[1], datos[2], datos[3], datos[4], datos[5], datos[6], datos[7], datos[8], datos[9], datos[10], "0"));
				}else{
					System.out.println(linea);
			    viajes.agregar((F) new VOTrip(datos[0], datos[1], datos[2], datos[3], datos[4], datos[5], datos[6], datos[7], datos[8], datos[9], datos[10], datos[11]));
				}
			}
	        br.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
        }
	}
	
	@Override
	public IDoublyLinkedList<VOTrip> getTripsOfGender (String gender) {
		// TODO Auto-generated method stub
		DoublyLinkedList<VOTrip> listaGenero = new DoublyLinkedList(null);		
			for (int i = 1; i < viajes.getSize()+1; i++) {
				VOTrip buenViaje=(VOTrip) viajes.darElemento(i);
				if(buenViaje.getGenero().equals(gender))
				{
					listaGenero.agregar(buenViaje);
					System.out.println(buenViaje.toString());
				}
			}			
		
		return listaGenero;
	}

	@Override
	public IDoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		// TODO Auto-generated method stub
		DoublyLinkedList<VOTrip> listaEstacion = new DoublyLinkedList(null);
		for (int i = 1; i < viajes.getSize(); i++) {
			VOTrip buenViaje=(VOTrip) viajes.darElemento(i);
			if(buenViaje.getIdEstacionFin()==stationID)
			{
				listaEstacion.agregar(buenViaje);
				
			}
		}
		return listaEstacion;
	}




}
