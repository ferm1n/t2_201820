package esqueleto_t2_201820.test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.DoublyLinkedList;
import model.data_structures.Node;

public class DoublyLinkedListTest<F> {

	private DoublyLinkedList<F> lista;
	Node<F> primero=new Node(5);
	Node<F> segundo=new Node(7);
	Node<F> tercero=new Node(11);
	Node<F> cuarto=new Node(435);
	
	private void setupEscenario(){
		
		lista=new DoublyLinkedList(primero);		
	}
	
	@Test
	public void testGetSize()
	{
		setupEscenario();
		assertEquals("El tama�o no corresponde", 1, lista.getSize());
	}
	
	@Test
	public void testAgregar()
	{
		setupEscenario();
		lista.agregar(segundo.darDato());
		lista.agregar(tercero.darDato());
		assertEquals("No se a�adieron correctamente los datos", 3, lista.getSize());
	}
	
	@Test
	public void testDarElemento()
	{
		setupEscenario();
		lista.agregar(segundo.darDato());
		lista.agregar(tercero.darDato());
		lista.agregar(cuarto.darDato());
		assertEquals("No se tiene en cuenta la posicion solicitada con respecto al tamanho de la lista",null,lista.darElemento(30));
		assertEquals("No se retorno el dato solicitado",tercero.darDato(),lista.darElemento(3));
	}
	
	
	
	
	
}
